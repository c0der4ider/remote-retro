defmodule RemoteRetroWeb.UserView do
  use RemoteRetroWeb, :view

  alias RemoteRetroWeb.UserView

  def render("index.json", %{users: users}) do
    %{users: render_many(users, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    user
  end
end
