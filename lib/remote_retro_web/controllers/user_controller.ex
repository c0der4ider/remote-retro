defmodule RemoteRetroWeb.UserController do
  use RemoteRetroWeb, :controller
  alias RemoteRetro.User

  # Rendering the file as .json, adding the complete list.
  def index(conn, _params) do
    render(conn, "index.json", users: get_user_list())
  end

  # Create a private function to fetch entries from User model.
  defp get_user_list do
    Repo.all(User)
  end
end
