defmodule RemoteRetro.UserControllerTest do
  use RemoteRetroWeb.ConnCase, async: true

  setup %{conn: conn} do
    # Via test to check json integrity, need to change the header for valid format (json).
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "authenticated user requests" do
    setup :authenticate_connection

    test " GET request to /users lists all users registered", %{conn: conn} do
      # Via user_path, extract data response.
      conn = get(conn, Routes.user_path(conn, :index))

      # Test to check pressence of users json block.
      assert json_response(conn, 200)["users"]

      # Test to refuse if data response is an empty json.
      refute json_response(conn, 200) == %{
               "users" => []
             }
    end
  end
end
